/*
 * 
 *  Copyright 2011 Li-Cheng (Andy) Tai
 *                 atai@atai.org
 *                 All Rights Reserved.
 *                               
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */

package org.atai.ImageViewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.atai.ImageViewer.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.TextView;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * @author atai
 *
 */
public class ImageViewerActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	the_act = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        image_uri = null;
        image_display = (ImageView) findViewById(R.id.image_display);
        image_display.setScaleType(ImageView.ScaleType.CENTER_CROP);
    	image_display_bitmap = ((BitmapDrawable)image_display.getDrawable()).getBitmap();
        mx = 0;
        my = 0;
        image_display.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View arg0, MotionEvent event) {

                float curX, curY;

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        mx = event.getX();
                        my = event.getY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        curX = event.getX();
                        curY = event.getY();
                        image_display.scrollBy((int) (mx - curX), (int) (my - curY));
                        mx = curX;
                        my = curY;
                        break;
                    case MotionEvent.ACTION_UP:
                        curX = event.getX();
                        curY = event.getY();
                        image_display.scrollBy((int) (mx - curX), (int) (my - curY));
                        break;
                }

                return true;
            }
        });
        
        result_display = (EditText) findViewById(R.id.result_display);
        select_button = (Button) findViewById(R.id.select_button);
        select_button.setOnClickListener(new OnClickListener() {
        	public void onClick(View v)
        	{
                Intent i = new Intent(Intent.ACTION_PICK );

            	i.setType("image/*");

            	Intent c = Intent.createChooser(i, "Select image file");
            	select_intent = c;
            	startActivityForResult(c, SELECT_REQUEST);

        	}        
        });
    	camera_intent = new Intent("android.media.action.IMAGE_CAPTURE");
        capture_button = (Button) findViewById(R.id.capture_button);
        capture_button.setOnClickListener(new OnClickListener() {
            public void onClick(View V)
            {
            	startActivityForResult(camera_intent, CAMERA_REQUEST);
            }
        });
        

        result_display.setText("");

        result_display.setMovementMethod(new ScrollingMovementMethod());
        registerForContextMenu(result_display);

        
        
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
    	super.onActivityResult(requestCode, resultCode, data);

    	if (RESULT_OK == resultCode)
    	{
    		
    		switch(requestCode)
    		{
    		case CAMERA_REQUEST: /* camera intent */
    			
	    		image_uri = data.getData();    				    		
	    		break;
    		case SELECT_REQUEST: 
	    		 /* intent was to select a static image */		    		
		        image_uri = data.getData();
	    		    		
	            break;
    		}

	        result_display.setText("");
    		

    	}
    	
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
          out.write(buffer, 0, read);
        }
    }

    protected void enable_buttons(boolean flag)
    {
    	select_button.setEnabled(flag);
    	recognize_button.setEnabled(flag);   
    	capture_button.setEnabled(flag);

    }
    
    
    protected void onPause()
    {
    	Drawable dr = image_display.getDrawable();
    	if (dr != null)
    	    image_display_bitmap = ((BitmapDrawable)dr).getBitmap();
    	else image_display_bitmap = null;
    	image_display.setImageBitmap(null);
    	super.onPause();
    }
    
    protected void onResume()
    {
    	if (image_uri != null)
    	{
    		try 
    		{
                if (image_display_bitmap != null) image_display_bitmap.recycle();
                image_display.setImageURI(image_uri);
                image_display_bitmap = ((BitmapDrawable)image_display.getDrawable()).getBitmap();
    		} 
    		catch (Error e)
    		{
    			AlertDialog a = new AlertDialog.Builder(the_act).create();
    			a.setTitle("Error");
    			a.setMessage("Image too big, cannot display");
    			a.setButton("OK", new DialogInterface.OnClickListener() {
    			      public void onClick(DialogInterface dialog, int which) {
    			       } }); 
    			a.show();
    			image_display_bitmap = null;
    		}
            image_uri = null;
    	}
    	else
    	    image_display.setImageBitmap(image_display_bitmap);
        super.onResume();
    }
    protected Button select_button, recognize_button, capture_button;
    protected EditText result_display;
    protected ImageView image_display;
    protected Bitmap image_display_bitmap;

    protected float mx, my;
    protected ImageViewerActivity the_act;
    protected Intent camera_intent, select_intent;
    protected ProgressDialog progress_indicator;
    
    protected Uri image_uri;
    protected static final int CAMERA_REQUEST = 0, SELECT_REQUEST = 1;
}