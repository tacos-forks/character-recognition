package org.atai.TessUI;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class TranslateActivity extends Activity {
	
	private boolean noNetwork=false;
	private String translation="";
	
	private Handler messageHandler = new Handler() {
        public void handleMessage(Message msg) {  
            switch(msg.what) {
            case 1:
            	if(translation.length()>0)
            	{
            		 Intent intent = new Intent();
                     intent.putExtra("translation",translation);
                     TranslateActivity.this.setResult(RESULT_OK, intent);
            	}
            	else
            	{
            		Intent intent = new Intent();
            		if(noNetwork)
                         intent.putExtra("translation","No network connection");
            		else
            			intent.putExtra("translation","unknown Error");
            		TranslateActivity.this.setResult(1, intent);
            	}
            	finish();
            	break;
            }
        }
	};
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        String [] tmp = getIntent().getStringArrayExtra("de.webxit.translatelibrary");
        if(tmp!=null)
        {
	        final String word = tmp[0];
	        final String from = tmp[1];
	        final String in = tmp[2];
	        
	        new Thread() {
		           public void run() {
		                try{
		                	translation = tranlsate(from, in, word);
		                	messageHandler.sendMessage(Message.obtain(messageHandler, 1)); 
		                } catch (Exception e) {  }
		           }
		    	}.start();
        }
        else
        {
        	Toast.makeText(this, "No data passed", Toast.LENGTH_SHORT).show();
        	finish();
        }
    }
    
    public String tranlsate(String from, String in, String word)
	{
		word = word.replace(" ", "%20").replace("&", "");
		String [] words = word.split("\n");
		
		String url = "https://ajax.googleapis.com/ajax/services/language/translate?v=1.0";
		
		int i =0;
		for (String string : words) {
			url = url+"&q="+string;
			i++;
		}
		url = url+"&langpair="+from+"|"+in;
		
		/**
		 * Abfrage ausführen
		 */
		String result = send(url).replace("\n", "");
		
		/**
		 * Ergebnis parsen
		 */
		JSONObject json;
		String ret= "";
		if(i!=1)
		{
			try {
				json = new JSONObject(result);
				JSONArray translations = json.getJSONArray("responseData");
				for (int i1 = 0; i1 < translations.length(); i1++) {
		            if (!translations.isNull(i1)) {
		                JSONObject item = translations.getJSONObject(i1);          
		                if (item.has("responseData")) {
		                	JSONObject item2 = item.getJSONObject("responseData");
		                	 if (item2.has("translatedText"))
		                		 ret = ret+ item2.getString("translatedText")+"\n";
		                }
		            }
		        }
				ret = ret.substring(0, ret.length()-1);
			} catch (Exception e) {
				e.printStackTrace();
			}  
		}
	    else
	    {
	    	try {
				json = new JSONObject(result);
				JSONObject jo = json.getJSONObject("responseData");
		        ret = jo.getString("translatedText");
			} catch (Exception e) {
				e.printStackTrace();
			}  
	    }
		return ret;
	}
    
	private String send(String url)
	{
		String result="";
		try {
	        URL url1 = new URL(url);
	    
	        BufferedReader in = new BufferedReader(new InputStreamReader(url1.openStream()));
	        String str;
	        while ((str = in.readLine()) != null) {
	        	result=result+"\n"+str;
	        }
	        in.close();
		}catch (UnknownHostException e) {
	    	this.noNetwork=true;
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
	    if(result.length()>0)
	    	return  result;
	    else
	    	return "error";
	}
}
