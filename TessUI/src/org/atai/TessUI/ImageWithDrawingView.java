/*
 *
 *  Copyright 2014 Li-Cheng (Andy) Tai
 *                 atai@atai.org
 *                 All Rights Reserved.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 *
 */
package org.atai.TessUI;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.ImageView;


public class ImageWithDrawingView extends TouchImageView {
    private TessUIActivity.RecognitionResults  recognized_items;
    public static final String TAG = "org.atai.TessUI.ImageWithDrawingView";
    private Paint yellow = new Paint(), black = new Paint(), blue = new Paint();
    private int text_base_line;
    private boolean mDrawOverlay = true;
    public ImageWithDrawingView(Context context) {
        super(context);
        initializePaints();
    }
    
    public ImageWithDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializePaints();
    }
    
    public ImageWithDrawingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initializePaints();
    }
    
    private void initializePaints() {
        yellow.setColor(Color.YELLOW);
        black.setColor(Color.BLACK);
        blue.setColor(Color.BLUE);
        yellow.setAlpha(0xaf);
        black.setAlpha(0xaf);
        blue.setAlpha(0x2f);
        yellow.setAntiAlias(true);
        black.setAntiAlias(true);
        blue.setAntiAlias(true);
        yellow.setStrokeWidth(2.0f);
        black.setStrokeWidth(4.0f);
        blue.setStrokeWidth(2.0f);
        yellow.setStyle(Paint.Style.STROKE);
        black.setStyle(Paint.Style.STROKE);
        blue.setStyle(Paint.Style.STROKE);
        
    }
    
    protected void drawTextWithinRect(Canvas canvas, String text, Rect rect, int fill_color, int outline_color) {
        if ((text == null) || (text.length() == 0))
            return;
        
        canvas.save();
        
        int width = rect.right - rect.left;
        int height = rect.bottom - rect.top;
        
        //adjust size
        TextPaint tpaint = new TextPaint();
        tpaint.setTextSize(100);
        tpaint.setTextScaleX(1.0f);
        tpaint.setColor(fill_color);
        Rect bounds = new Rect();
        tpaint.getTextBounds(text, 0, text.length(), bounds);
        int h = bounds.bottom - bounds.top;
        float target = (float) (height * 0.9f);
        float size = (target / h ) * 100f;
        tpaint.setTextSize(size);
        
        // adjust X scale
        tpaint.setTextScaleX(1.0f);
        tpaint.getTextBounds(text, 0, text.length(), bounds);
        int w = bounds.right - bounds.left;
        int text_h = bounds.bottom - bounds.top;
        text_base_line = bounds.bottom + ((height - text_h) / 2);
        float xscale = ((float)(width)) / w;
        tpaint.setTextScaleX(xscale);
        
        tpaint.setStyle(Style.STROKE);
        tpaint.setStrokeJoin(Join.ROUND);
        tpaint.setStrokeMiter(10);
        tpaint.setStrokeWidth(3);
        tpaint.setColor(outline_color);
        canvas.drawText(text, rect.left/* + width / 2*/, rect.top + height - text_base_line, tpaint);
        
        tpaint.setColor(fill_color);
        tpaint.setStyle(Style.FILL);
        canvas.drawText(text, rect.left/* + width / 2*/, rect.top + height - text_base_line, tpaint);
        
        canvas.restore();
    }
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mDrawOverlay) {
            canvas.save();
            Matrix m = getImageMatrix();
            canvas.concat(m);
            if (recognized_items  != null) {
                int n = recognized_items.size();
                for (int i = 0; i < n; i++) {
                    TessUIActivity.RecognizedText item = recognized_items.get(i);
                    
                    canvas.drawRect(item.rect, blue);
                    canvas.drawRect(item.rect, yellow);
                    
                    drawTextWithinRect(canvas, item.text, item.rect, yellow.getColor(), blue.getColor());
                    
                }
            }
            canvas.restore();
        }
        
    }
    
    public void setRecognitionResults(TessUIActivity.RecognitionResults items) {
        recognized_items = items;
        invalidate();
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (mDrawOverlay)
                setImageAlpha(0x00);
            else setImageAlpha(0x7f);
        }*/
    }
    
    public void setDrawOverlay(boolean b) {
        mDrawOverlay = b; 
        invalidate();
    }
    
};